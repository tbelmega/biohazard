package de.belmega.biohazard.server.printing;

/**
 * @author tbelmega on 08.01.2017.
 */
public class TestBean {
    String name;

    public TestBean(String s) {
        name = s;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
